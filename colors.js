define(function(){
	var Colors = {
		base 			: '#FF8A3F',
		base_light 		: 'rgb(253, 222, 202)',
		base_lighter 	: 'lighten(@base_light, 8%)',
		green 			: 'rgb(134, 202, 107)',
		red 			: 'rgb(202, 103, 107)',
		google_red 		: '#dd4b39',
		blue 			: 'rgb(107, 134, 202)',
		facebook_blue 	: '#3b5998',
		twitter_blue 	: '#55acee',
		instagram_brown : '#9b6954',
		gray 			: 'rgb(200, 200, 200)',
		gray_dark 		: 'rgb(150, 150, 150)',
		gray_darker 	: 'rgb(130, 130, 130)'
	};

	return Colors;
});

